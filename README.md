有些是自已写的，有些是互联网查找出来，主要是为了方便可以马上用


/**
 * 删除网页上看不见的隐藏字符串, 如 Java\0script
 *
 * @param   string
 */
remove_invisible_characters(&$str, $url_encoded = TRUE)

/**
 * 对数组或字符串进行转义处理，数据可以是字符串或数组及对象
 * @param void $data
 * @return type
 */
addslashes_d($data)
/**
 * 去除转义
 * @param type $data
 * @return type
 */
stripslashes_d($data)
/**
 * 将数组转为字符串表示形式
 * @param array $array 数组
 * @param int $level 等级不要传参数
 * @return string
 */
array_to_String($array, $level = 0)

/**
 * 产生随机字串，可用来自动生成密码 默认长度6位 字母和数字混合
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
 * @return string
 */
rand_string($len=6,$type='',$addChars='')

// 随机生成一组字符串
build_count_rand ($number,$length=4,$mode=1) 
rand_email()
rand_phone()
rand_user()
/**
 * 检查字符串是否是UTF8编码
 * @param string $string 字符串
 * @return Boolean
 */
is_utf8($string) 
/**
 * php 获取一定范围内不重复的随机数字，在1-10间随机产生5个不重复的值
 * @param int $begin
 * @param int $end
 * @param int $limit
 * @return array
 */
getRand($begin=0,$end=10,$limit=5)
/**
 * [machUrl 是否为URL]
 * @Author   Jerry
 * @DateTime 2017-11-04T01:16:49+0800
 * @Example  eg:
 * @param    [type]                   $str [description]
 * @return   [type]                        [description]
 */
matchUrl($str)
/**
 * [matchImage 检查字符串中是否有图片]
 * @Author   Jerry
 * @DateTime 2017-11-04T01:09:58+0800
 * @Example  eg:
 * @param    [type]                   $str [description]
 * @return   [type]                        [description]
 */
matchImage($str)
/**
 * [GrabImage 保存远程图片至本地]
 * @param [type] $url      [远程图片地址]
 * @param string $filename [保存图片名]
 */
GrabImage($url,$filename="") 
/**
 * 获取url
 * @return [type] [description]
 */
getUrl()
/**
 * 下载
 * @param  [type] $filename [description]
 * @param  string $dir      [description]
 * @return [type]           [description]
 */
downloads($filename,$dir='./')


/**
 * 打印输出数据
 * @param void $var
 */
show($var) 

    /**
     * 加载静态资源
     * @param string $static 资源名称
     * @param string $type 资源类型
     * @author 
     * @return string
     */
    load_static($static = '', $type = 'css',$minify='true')
   

    /**
     * 合并输出js代码或css代码 需要minify插件支付
     * @param string $type 类型：group-分组，file-单个文件，base-基础目录
     * @param string $files 文件名或分组名
     * @author 
     */
    minify($type = '',$files = '')


    /**
     * 获取浏览器类型
     * @return string
     */
get_browser_type()

/**
 * 判断是否是合格的手机客户端
 * 
 * @return boolean
 */
is_mobile()

/**
 * 判断是否处于微信内置浏览器中
 * 
 * @return boolean
 */
in_weixin()

    /**
     * 生成随机字符串
     * @param int $length 生成长度
     * @param int $type 生成类型：0-小写字母+数字，1-小写字母，2-大写字母，3-数字，4-小写+大写字母，5-小写+大写+数字
     * @author 蔡伟明 <314013107@qq.com>
     * @return string
     */
    generate_rand_str($length = 8, $type = 0) 

    /**
     * 获取服务器端IP地址
     * @return array|false|string
     */
    get_server_ip()
/**
 * [getIp 此方法相对比较好]
 * @return [type] [description]
 */
fetch_ip()
/**
 * 验证 IP 地址是否为内网 IP
 * 
 * @param string
 * @return string
 */
valid_internal_ip($ip)

/**
 * 兼容性转码
 * 
 * 系统转换编码调用此函数, 会自动根据当前环境采用 iconv 或 MB String 处理
 * 
 * @param  string
 * @param  string
 * @param  string 
 * @return string
 */
convert_encoding($string, $from_encoding = 'GBK', $target_encoding = 'UTF-8')

/**
 * 兼容性转码 (数组)
 * 
 * 系统转换编码调用此函数, 会自动根据当前环境采用 iconv 或 MB String 处理, 支持多维数组转码
 * 
 * @param  array
 * @param  string
 * @param  string 
 * @return array
 */
convert_encoding_array($data, $from_encoding = 'GBK', $target_encoding = 'UTF-8')

/**
 * 字符串截取，支持中文和其他编码
 * @param  [string]  $str     [字符串]
 * @param  integer $start   [起始位置]
 * @param  integer $length  [截取长度]
 * @param  string  $charset [字符串编码]
 * @param  boolean $suffix  [是否有省略号]
 * @return [type]           [description]
 */
msubstr($str, $start=0, $length=50, $charset="utf-8", $suffix=true)
/**
 * 递归创建目录
 * 
 * 与 mkdir 不同之处在于支持一次性多级创建, 比如 /dir/sub/dir/
 * 
 * @param  string
 * @param  int
 * @return boolean
 */
make_dir($dir, $permission = 0777)

/**
 * jQuery jsonp 调用函数
 * 
 * 用法同 json_encode
 * 
 * @param  array
 * @param  string
 * @return string
 */
jsonp_encode($json = array(), $callback = 'jsoncallback')

/**
 * 求两个日期之间相差的天数
 * (针对1970年1月1日之后，求之前可以采用泰勒公式)
 * @param string $day1
 * @param string $day2
 * @return number
 */
diffDate ($second1, $second2)

/**
 * 时间友好型提示风格化（即微博中的XXX小时前、昨天等等）
 * 
 * 即微博中的 XXX 小时前、昨天等等, 时间超过 $time_limit 后返回按 out_format 的设定风格化时间戳
 * 
 * @param  int
 * @param  int
 * @param  string
 * @param  array
 * @param  int
 * @return string
 */
date_friendly($timestamp, $time_limit = 604800, $out_format = 'Y-m-d H:i', $formats = null, $time_now = null)

/**
 * 时间差计算
 *
 * @param Timestamp $time
 * @return String Time Elapsed
 * @author Shelley Shyan
 * @copyright http://phparch.cn (Professional PHP Architecture)
 */
time2Units ($time)


/**
 * 获得几天前，几小时前，几月前
 * @param int $time 时间戳
 * @param array $unit 时间单位
 * @return bool|string
 */
date_before($time, $unit = null)
/**
 * 根据一个时间戳得到详细信息
 * @param  [type] $time [时间戳]
 * @return [type]      
 * @author [yangsheng@yahoo.com]
 */
getDateInfo($time)
/**
 * 获得日期是上中下旬
 * @param  [int] $j [几号]
 * @return [array]    [description]
 * @author [yangsheng@yahoo.com]
 */
getTenDays($j)

/**
 * 根据月份获得当前第几季度
 * @param  [int] $n [月份]
 * @param  [int] $y [年]
 * @return [array]    [description]
 */
getQuarter($n,$y=null)

/**
 * 判断文件或目录是否可写
 * 
 * @param  string
 * @return boolean
 */
is_really_writable($file)

/**
 * 生成密码种子
 * 
 * @param  integer
 * @return string
 */
fetch_salt($length = 4)


/**
 * 根据 salt 混淆密码
 *
 * @param  string
 * @param  string
 * @return string
 */
compile_password($password, $salt)

/**
 * 获取数组中随机一条数据
 * 
 * @param  array
 * @return mixed
 */
array_random($arr)

/**
 * 递归读取文件夹的文件列表
 * 
 * 读取的目录路径可以是相对路径, 也可以是绝对路径, $file_type 为指定读取的文件后缀, 不设置则读取文件夹内所有的文件
 * 
 * @param  string
 * @param  string
 * @return array
 */
fetch_file_lists($dir, $file_type = null)

/**
 * CURL 获取文件内容
 * 
 * 用法同 file_get_contents
 * 
 * @param string
 * @param integerr
 * @return string
 */
curl_get_contents($url, $timeout = 10)

/**
 * CURL 获取文件内容
 * 
 * 用法同 curl_post_contents
 *  $url = "http://localhost/web_services.php";
 *  $post_data = array ("username" => "bob","key" => "12345");
 * @param string
 * @param integerr
 * @return string
 */
curl_post_contents($url = '', $param = '')
curl_multi($url)

/**
 * “抽奖”函数
 *
 * @param integer $first    起始编号
 * @param integer $last     结束编号
 * @param integer $total    获奖人数
 *
 * @return string
 *
*/
isWinner($first, $last, $total)
{
    $winner = array();
    for ($i=0;;$i++)
    {
        $number = mt_rand($first, $last);
        if (!in_array($number, $winner))
            $winner[] = $number;    // 如果数组中没有该数，将其加入到数组
        if (count($winner) == $total)   break;
    }
    return implode(' ', $winner);
}
 /*********************************************************************
    $token = encrypt($id, 'E', 'qingdou');
    echo '加密:'.encrypt($id, 'E', 'qingdou');
    echo '<br />';
    echo '解密：'.encrypt($token, 'D', 'qingdou');
    函数名称:encrypt
    函数作用:加密解密字符串
    使用方法:
    加密     :encrypt('str','E','qingdou');
    解密     :encrypt('被加密过的字符串','D','qingdou');
    参数说明:
    $string   :需要加密解密的字符串
    $operation:判断是加密还是解密:E:加密   D:解密
    $key      :加密的钥匙(密匙);
    *********************************************************************/
    encrypt($string,$operation,$key='')

/**
 * 简单对称加密算法之加密
 * @param String $string 需要加密的字串
 * @param String $skey 加密EKY
 * @return String 加密后的字符串
 * @author Anyon Zou <cxphp@qq.com>
 */
encode($string = '', $skey = '6f918e') 
 
/**
 * 简单对称加密算法之解密
 * @param String $string 需要解密的字串
 * @param String $skey 解密KEY
 * @return String 解密后的字符串
 * @author Anyon Zou <cxphp@qq.com>
 */
decode($string = '', $skey = '6f918e') 

//=============================插件类 核心方法=====================================
/**
 * 1、电信手机访问，HTTP头会有手机号码值，移动、联通则无。
 *2、文中所提到的插入代码即可获取，纯属子虚乌有，文中的功能是一些做移动网络服务的公司，先向电信、移动、联通官方购买查询接口，该接口是以类似统计代码形式插入到你的网站，然后会有个后台统计系统。最后向其他公司贩卖会员，按数据条数收钱（重复也算），奇贵无比，每次最少续费三万。
 *3、只有移动网络有效（电信手机、移动、联通），其他方式访问无效。
 *（2013-8-16 10:43:10 核总补充：手机型号则是使用 HTTP 头 User-Agent 判断的，非常简单的“技术”，和普通网站程序判断浏览器型号及系统类型的方法一摸一样。）
 *该思路、系统最出自于医疗行业，未来移动互联网是发展方向，估计会扩展到其他行业。
 * [getPhoneNumber 获取访问的手机号码]
 * @return [type] [description]
 */
getPhoneNumber()


/**
 * 字节格式化 把字节数格式为 B K M G T 描述的大小
 * @return string
 */
byte_format($size, $dec=2) 

/**
 * [get_csv_contents 读取CSV文件，EXCEL处理]
 * @param  [type] $file_target [description]
 * @return [type]              [description]
 */
get_csv_contents( $file_target )
 
/**
 * [outputCsv 导出CSV]
 * @param  [type] $data [字符型]
 * @return [type]       [description]
 * $data .= i($result[$i]['name']).','.i($result[$i]['option'])."\n";  换行用\n
 */
outputCsv($str)

/**
 * [val PHP自带的验证规则]
 * @param  [type] $value [description]
 * @return [type]        [description]
 * FILTER_CALLBACK  调用用户自定义函数来过滤数据。
*FILTER_SANITIZE_STRING     去除标签，去除或编码特殊字符。
*FILTER_SANITIZE_STRIPPED   “string” 过滤器的别名。
*FILTER_SANITIZE_ENCODED    URL-encode 字符串，去除或编码特殊字符。
*FILTER_SANITIZE_SPECIAL_CHARS  HTML 转义字符 ‘”<>& 以及 ASCII 值小于 32 的字符。
*FILTER_SANITIZE_EMAIL  删除所有字符，除了字母、数字以及 !#$%&’*+-/=?^_`{|}~@.[]
*FILTER_SANITIZE_URL    删除所有字符，除了字母、数字以及 $-_.+!*’(),{}|\\^~[]`<>#%”;/?:@&=
*FILTER_SANITIZE_NUMBER_INT     删除所有字符，除了数字和 +-
*FILTER_SANITIZE_NUMBER_FLOAT   删除所有字符，除了数字、+- 以及 .,eE。
*FILTER_SANITIZE_MAGIC_QUOTES   应用 addslashes()。
*FILTER_UNSAFE_RAW  不进行任何过滤，去除或编码特殊字符。
*FILTER_VALIDATE_INT    在指定的范围以整数验证值。
*FILTER_VALIDATE_BOOLEAN    如果是 “1″, “true”, “on” 以及 “yes”，则返回 true，如果是 “0″, “false”, “off”, “no” 以及 “”，则返回 false。否则返回 NULL。
*FILTER_VALIDATE_FLOAT  以浮点数验证值。
*FILTER_VALIDATE_REGEXP     根据 regexp，兼容 Perl 的正则表达式来验证值。
*FILTER_VALIDATE_URL    把值作为 URL 来验证。
*FILTER_VALIDATE_EMAIL  把值作为 e-mail 来验证。
*FILTER_VALIDATE_IP     把值作为 IP 地址来验证。
 */
filter($str,$type)

/**
 * [num_to_rmb 数据转成大写]
 * @Author   Jerry
 * @DateTime 2018-01-22T16:22:44+0800
 * @Example  eg:
 * @param    [type]                   $num [description]
 * @return   [type]                        [description]
 */
num_to_rmb($num)

/**
 * [hidecard 隐藏敏感信息内容]
 * @Author   Jerry
 * @DateTime 2017-12-27T11:40:37+0800
 * @Example  eg:
 * @param    [type]                   $phone [description]
 * @return   [type]                          [description]
 */
hidecard($cardnum, $type = 1, $default = "")


