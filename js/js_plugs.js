

/***** 固定定位使用方法 *****/
/*
* 代码示例：$('#houdun').hd_fixed({'left':'10px','bottom':'20px'});
* 以上代码会将#houdun这个元素定位到可视区域距离左边10px，距离下边20px的位置。兼容ie6等低版本ie浏览器
*/

/***** 抽取随机数的方法 *****/
/*
* 本方法可以实现抽取任意两个数之间的随机小数或随机整数的效果。
* 代码示例：
* 1、 hd_random(23,66);
* 上面这段代码抽取的是23至66之间的随机小数（取不到23和66）
* 2、hd_random(23,66,true);
* 上面这段代码抽取的是23至66之间的随机整数（能取到23和66）
*/
/***** 抽取随机数的方法 *****/


/***** 计算倒计时的方法 *****/
/*
* 代码示例：
* 1、hd_countdown(2016,10,1,6,12,50);
* 以上代码是获得当前时间与2016年10月1日6时12分50秒的时间间隔
* 2、hd_countdown(1399316708);
* 以上代码传入的是以毫秒表示的utc时间戳，该时间戳表示1970年1月1日0时0分0秒这个参考点往后1399316708秒的时间。因为php给前台返回的时间一般是以这种时间戳的格式。
*/




/******************** 处理固定定位效果 ********************/
;(function($){ 
	$.extend($.fn,{ 

		hd_fixed : function(position){ 
			if(state = 'fixed'){//如果是一直固定状态
				var _this = $(this);
				// 如果是ie6
				if(!-[1,]&&!window.XMLHttpRequest){
					// alert($(this).attr('src'))
					$('body').append($(this));//将对象插入到body中
					$(this).css('position','absolute');//给元素加固定定位，相对于body进行定位

					hd_fixed_postion($(this),position);
					$(window).scroll(function(){
						hd_fixed_postion(_this,position);
					})
				}else{
					$(this).css('position','fixed');//让元素固定定位
					for(x in position){
						$(this).css(x,position[x]);
					}
				}
				
			}
			return $(this);
		} 

	}) 
})(jQuery); 

// 固定定位用到的函数
function hd_fixed_postion(obj,position){
	// alert(obj.attr('src')
			var window_height = $(window).height();//获得页面总高度
			var obj_height = obj.outerHeight();//获得定位元素的高度
			var scroll_top = $(document).scrollTop();//获得已滚动上去的高度
					var l,r,t,b;
					// for in遍历开始
					for(x in position){//遍历位置信息对象，将位置设置给obj对象
						switch(x){
							case 'left':
								l = position[x];
								break;
							case 'right':
								r = position[x];
								break;
							case 'top':
								t = scroll_top + parseInt(position[x]);
								break;
							case 'bottom':
								b = window_height + scroll_top - obj_height - parseInt(position[x]);
								break;
						}
					}
					// for in遍历结束
					if(l){
						obj.css('left',l);
					}
					if(r){
						obj.css('right',r);
					}
					if(b){
						obj.css('top',b);
					}
					if(t){
						obj.css('top',t);
					}
		}
//固定定位用到的函数结束
/******************** 处理固定定位效果结束 ********************/


// 抽取随机数的函数
function hd_random(start,end,integer){
	if(integer){//生成随机整数
		var random = Math.floor(Math.random()*(end+1-start)+start)
	}else{//生成随机小数
		var random = Math.random()*(end-start)+start;
	}
	return random; 
}
// 抽取随机数的函数结束


// 倒计时功能
function hd_countdown(year,month,day,hour,minute,second){

		var start_time = new Date();//创建现在时间


		if(arguments.length == 6){//如果传入6个参数，就是传入的年月日时分秒
			var end_time = new Date(year,month-1,day,hour,minute,second);//创建未来时间
			var diff = end_time.getTime() - start_time.getTime();//获得两个时间的时间差
		}else if(arguments.length == 1){//如果只传入1个参数，就是传入的时间戳
			var diff = year*1000 - start_time.getTime();//获得两个时间的时间差
		}
		
		
		var result = new Object();//创建用来记录结果的对象

		result.days= parseInt(diff/(24*60*60*1000));//计算剩余天数
		diff = diff%(24*60*60*1000);//计算还剩多少毫秒
		result.hours = parseInt(diff/(60*60*1000));//计算剩余小时
		diff = diff%(60*60*1000);
		result.minutes = parseInt(diff/(60*1000));//计算还剩多少分钟
		diff = diff%(60*1000);
		result.seconds = parseInt(diff/1000);//计算还剩多少秒

		return result;//返回结果

}

// 倒计时功能结束
// // 添加浏览器书签
function addBookmark(){
    var title = document.title;
    var URL = document.URL;

    try {
        window.external.addFavorite(URL, title);          //ie
    } catch(e) {
        try {
              window.sidebar.addPanel(title, URL, "");     //firefox
        } catch(e) {
              alert("该浏览器不支持，请使用Ctrl+D进行添加");     //chrome opera safari
        }
    }
} 

/**
 * [SetHome 设为主页]
 * @param {[type]} obj [description]
 * @param {[type]} vrl [description]
 */
  function SetHome(obj, vrl) {
        try {
            obj.style.behavior = 'url(#default#homepage)'; obj.setHomePage(vrl);
            NavClickStat(1);
        }
        catch (e) {
            if (window.netscape) {
                try {
                    netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
                }
                catch (e) {
                    alert("抱歉！您的浏览器不支持直接设为首页。请在浏览器地址栏输入“about:config”并回车然后将[signed.applets.codebase_principal_support]设置为“true”，点击“加入收藏”后忽略安全提示，即可设置成功。");
                }
                var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
                prefs.setCharPref('browser.startup.homepage', vrl);
            }
        }
    }
